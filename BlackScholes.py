from math import sqrt, log, exp, erf
import random
from numpy import arange
import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
import plotly
from plotly.graph_objs import *
from scipy.stats import norm

S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]  # Exercise prices range
T = 1  # T = Time to expiration
r = 0.01  # r = risk-free interest rate
q = 0.02  # q = dividend yield
vol = 0.2  # vol = volatility
Nsteps = 100  # Number or steps in MC

###### First option: Black-Scholes
def d1(S0, K, r, q, sigma, T):
    return (log(S0/K)+(r-q+sigma**2/2)*T)/(sigma*sqrt(T))

def d2(S0, K, r, q, sigma, T):
    return (log(S0/K)+(r-q+sigma**2/2)*T)/(sigma*sqrt(T))

xd1 = []
xd2 = []
for i in range(0, 99):
   xd1.append(d1(S0, strikes[i], r, q, 2, T))
   xd2.append(d2(S0, strikes[i], r, q, 2, T))
x = norm.cdf(xd1)
y = norm.cdf(xd2)

def C0(S0, x, K, r, T, y):
    return S0*x - K*exp(-r*T)*y # x stands for d1 and y stands for d2

price = []
for i in range(0,99):
    price.append(C0(S0, x[i], strikes[i], r, T, y[i]))

col1 = [i for i in range(1,100)]
MyDataFrame = pd.DataFrame({'INDEX':col1,
                            'd1':x,
                            'd2':y,
                            'price':price})
MyDataFrame['price'].plot()
print(price)
plt.show()




###### Third option: load the dataset
df = pd.read_csv('Data/BlackScholes.csv', delimiter=";")
BS = df[np.isfinite(df['OPTIONVAL'])]
BS['OPTIONVAL'].plot()
#plt.show()

#LayoutGraph = Layout(title="Price", xaxis=XAxis(title="Year"), yaxis=YAxis(title="Price"))
#FigureGraph = Figure(data=BS['OPTIONVAL'], layout=LayoutGraph)
#plotly.offline.plot(FigureGraph,filename="Price.html")